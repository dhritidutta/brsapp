import { Component, OnInit,Inject} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import{OrderbookComponent}from '../orderbook/orderbook.component';
import{MarketwatchComponent} from "../marketwatch/marketwatch.component";
import{ChartoneComponent} from "../chartone/chartone.component";
import * as $ from "jquery";
import { from } from 'rxjs';
import {FormControl, Validators} from '@angular/forms';
import {NgForm} from '@angular/forms';


export interface DialogData {
  animal: string;
  name: string;
}
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  animal: string;
  name: string;
 // Email:string;
 // public Password:any;
  
  // demo_html = require('!!html-loader!./navbar.component.html');
  // demo_ts = require('!!raw-loader!./navbar.component.ts');
  // demo_scss = require('!!raw-loader!./navbar.component.css');


  constructor(public dialog: MatDialog,public _OrderbookComponent:OrderbookComponent,public _MarketwatchComponent:MarketwatchComponent,public _ChartoneComponent:ChartoneComponent) { }

  ngOnInit(): void {
  }
  

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '535px',
      data: {name: this.name, animal: this.animal}
    });

    dialogRef.afterClosed().subscribe(result => {
    
      console.log('The dialog was closed');
      this.animal = result;
    });
  }
  // Opendatawindow(){
 
    
  // }
  Openwatchlist(){
    $("#daataTab").style.display='block';
  }

}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'login-dialog.html',
})
export class DialogOverviewExampleDialog {
  
 public Email:any;
 public Pass:any;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
 
}