import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import{NavbarComponent} from "../app/navbar/navbar.component";
import{OrderbookComponent} from "../app/orderbook/orderbook.component";
import{LayoutComponent} from "../app/layout/layout.component";
import{LoginComponent} from "../app/login/login.component";

const routes: Routes = [
  { path: 'test', component: NavbarComponent },
  { path: 'orderbook', component: OrderbookComponent },
  { path: 'layout', component: LayoutComponent },
  { path: 'login', component: LoginComponent }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
