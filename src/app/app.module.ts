import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularDraggableModule } from 'angular2-draggable';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon'; 
import {MatMenuModule} from '@angular/material/menu';
import { LayoutComponent } from './layout/layout.component';
import { OrderbookComponent } from './orderbook/orderbook.component';
import { WatchlistComponent } from './watchlist/watchlist.component';
import {MatButtonModule} from '@angular/material/button';
import { MarketwatchComponent } from './marketwatch/marketwatch.component';
import { ChartoneComponent } from './chartone/chartone.component';
import { CharttwoComponent } from './charttwo/charttwo.component';
import{DialogOverviewExampleDialog} from '../app/navbar/navbar.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    LayoutComponent,
    OrderbookComponent,
    WatchlistComponent,
    MarketwatchComponent,
    ChartoneComponent,
    CharttwoComponent,
    DialogOverviewExampleDialog,
 
   
  ],
  imports: [ReactiveFormsModule ,MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,MatToolbarModule,MatIconModule,MatMenuModule,MatButtonModule,AngularDraggableModule,FormsModule,MatDialogModule
  ],
  exports: [MatSelectModule,MatFormFieldModule,MatToolbarModule,MatIconModule,MatToolbarModule,MatMenuModule,MatButtonModule,MatDialogModule],
  entryComponents:[DialogOverviewExampleDialog],
  providers: [OrderbookComponent,MarketwatchComponent,ChartoneComponent,ChartoneComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
