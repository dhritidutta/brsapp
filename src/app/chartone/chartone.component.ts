import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chartone',
  templateUrl: './chartone.component.html',
  styleUrls: ['./chartone.component.css']
})
export class ChartoneComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  closewindow(){
    document.getElementById("chartclose").style.display='none'; 
  }
  openchartone(){
    document.getElementById("chartclose").style.display='block'; 
  }
}
